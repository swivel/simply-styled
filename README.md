# simply-styled

Simple, dependency-free CSS-in-JS.

Fully-ESM compliant and ready for the browser.

## Examples

### Straight-forward

```jsx
// components/nav.js
import React from 'react';
import styled from 'simply-styled';

export const navStyle = styled("nav")`
	background: #ccc;
	padding: 1rem;
	font-size: 1rem;
`;

export default ({ children }) => (
	<nav className={navStyle}>
		{children}
	</nav>
);
```

### With child selectors

```jsx
// components/nav.js
import React from 'react';
import styled from 'simply-styled';

export const navStyle = styled("nav")`
	background: #ccc;
	padding: 1rem;
	font-size: 1rem;
`;

navStyle.child("a")`
	display: inline-block;
	color: #222222;
	text-decoration: none;
`;

export default ({ children }) => (
	<nav className={navStyle}>
		{children}
	</nav>
);
```

### Inheritance

Inheritance works out of the box, and always inherits child selectors styles.

```jsx
// components/nifty-nav.js
import React from 'react';
import styled from 'simply-styled';
import { navStyle } from 'components/nav';

export const niftyNav = styled(navStyle)`
	border: 1px solid #f00;
	border-radius: 6px;
`;

export default ({ children }) => (
	<nav className={niftyNav}>
		<a href="#">I'm still an inline-block!</a>
	</nav>
);
```


### Psuedo selectors

You can also use psuedo-selectors for your primary element

```jsx
// components/nifty-nav.js
import React from 'react';
import styled from 'simply-styled';
import { navStyle } from 'components/nav';

export const niftyNav = styled(navStyle)`
	border: 1px solid ${({ borderColor }) => borderColor};
	border-radius: 6px;
`;

niftyNav.child('::before')`
	content: " ";
	display: inline-block;
`;

export default ({ children, ...props }) => (
	<nav className={niftyNav.with(props)}>
		<a href="#">I'm still an inline-block!</a>
	</nav>
);
```


### With props

Now, you can pass props to your stylesheets

```jsx
// components/nifty-nav.js
import React from 'react';
import styled from 'simply-styled';
import { navStyle } from 'components/nav';

export const niftyNav = styled(navStyle)`
	border: 1px solid ${({ borderColor }) => borderColor};
	border-radius: 6px;
`;

export default ({ children, ...props }) => (
	<nav className={niftyNav.with(props)}>
		<a href="#">I'm still an inline-block!</a>
	</nav>
);
```


