import styled from "../index.js";

window.className = styled("foo")`
	background: #ffffff;
`;
	console.log(className);
	console.log(`${className}`);
	console.log(className.toCSS());

window.black = "#000000";
window.otherClassName = styled(className)`
	color: ${black};
`;
	console.log(otherClassName);
	console.log(`${otherClassName}`);
	console.log(otherClassName.toCSS());

window.funcClassName = styled(otherClassName)`
	border: 1px solid ${black};
	outline: 5px ${black};
`;
	console.log(funcClassName);
	console.log(`${funcClassName}`);
	console.log(funcClassName.toCSS());

window.overClassName = styled(funcClassName)`
	border: 5px solid #00f;
`;
	console.log(overClassName);
	console.log(`${overClassName}`);
	console.log(overClassName.toCSS());



const withProps = styled("withprops")`
	background: #ffffff;
	color: ${black};
	border: 5px solid ${({ color = black }) => color};
`;
	console.log(withProps);
	console.log(`${withProps}`);
	console.log(withProps.toCSS());

className.child("h1")`
	color: #00f;
`;

className.child("&:hover, h1")`
	background: #000 !important;
	justify-content: ${() => "middle"};
`;

window.div = document.createElement("div");
	div.classList.add(funcClassName);
	div.innerHTML = "<h1>Oh, hello there!</h1>";
	document.body.appendChild(div);

function foo(color) {
	div.classList.add(withProps.with({ color }));
}
foo('#0f0');
foo('#aaf');

function boo() {
	window.ddiv = document.createElement("div");
		ddiv.classList.add(withProps.with({ color: "#0f0" }));
		ddiv.classList.add(overClassName);
		ddiv.innerHTML = "<h1>Oh, hello there!</h1>";
		document.body.appendChild(ddiv);
}
boo();

className.child("h1")`
	background: ${({ color }) => color ?? "#f00"};
`;
