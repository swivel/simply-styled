/**
 *  styled-js: A dependency-free CSS-in-JS library
 *
 *  Copyright (C) 2020  Joseph Dalrymple
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

import StyleContext from "./style-context.js";

export default class StyleClass {
	constructor(className, styles, parent) {
		this.descendents = [];
		if (parent) parent._desc(this);

		this.className = className;
		this.parent = parent;

		this.needsProp = Array.from(styles).findIndex(s => typeof s === "function") >= 0;

		this.contexts = new Map();

		this._children = new Map();
		this._styles = styles;

		this._render();
	}

	get children() {
		const { parent, _children } = this;
		return new Map([
			...(parent ? parent.children : []),
			..._children
		]);
	}

	get styles() {
		const { parent, _styles } = this;
		return [
			...(parent ? parent.styles : []),
			..._styles
		];
	}

	child(selector) {
		return (css, ...props) => {
			const styles = [
				...(
					this._children.has(selector)
						? this._children.get(selector)
						: []
				),
				...css.reduce((s, c, i) => ([
					...s, c, ...([
						props[i] ?? ""
					])
				]), [])
			];

			this._children.set(selector, styles);
			this._render();
		}
	}

	with(props = {}) {
		if (!this.needsProp) {
			throw new RangeError('simply-styled::StyleClass.with( props ): This stylesheet does not require any properties.');
		}

		const context = StyleContext.generateContextId(this.className);

		if (this.contexts.has(context)) {
			const ctx = this.contexts.get(context);
			ctx.update(props);
			return ctx;
		}

		const ctx = new StyleContext(props, this, context);
		this.contexts.set(context, ctx);

		return ctx;
	}
	

	toString() {
		return this.className;
	}

	toCSS() {
		return this._arrToCSS(this.styles);
	}

	_arrToCSS(styles = this.styles, props = {}) {
		return Array.from(styles).reduce((s, v) => (
			`${s}${typeof v === "function" ? v(props) : v}`
		), "");
	}

	_desc(desc) {
		if (!(desc instanceof StyleClass)) return;
		this.descendents.push(desc);
	}

	_formatSelector(selector, className = this.className) {
		let final = [];
		const parts = selector.split(",").map(s => s.trim());

		for (let piece of parts) {
			const separator = piece[0] === "&" ? "" : " ";
			const trimmed = piece[0] === "&" ? piece.slice(1) : piece;
			const formatted = trimmed.replace("&", `.${className}`);
			final.push(`.${className}${separator}${formatted}`);
		}

		return final.join(",\n");
	}

	_render() {
		this.descendents.forEach(d => d._render());

		if (this.needsProp) {
			for (let [context, ctxInst] of this.contexts) {
				ctxInst._render();
			}

			return;
		}

		const newElem = document.createElement("style");

		const { className } = this;
		newElem.innerHTML = `.${className} \{\n${this.toCSS()}\n\}`;

		for (let [sel, styles] of this.children) {
			const selector = this._formatSelector(sel);
			newElem.innerHTML += `\n\n${selector} \{\n${this._arrToCSS(styles)}\n\}`;
		}

		if (document.body.contains(this.elem)) {
			this.elem.replaceWith(newElem);
		} else {
			document.body.appendChild(newElem);
		}

		this.elem = newElem;
	}
}

