/**
 *  styled-js: A dependency-free CSS-in-JS library
 *
 *  Copyright (C) 2020  Joseph Dalrymple
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

export const toHex = i => Math.abs(i).toString(16);

export const genClassName = (prefix = null) => {
	const prepend = (typeof prefix === "string" && prefix.length > 0) ? (
		`${prefix}_`
	) : "";

	const arr = new Uint8Array(3);
	crypto.getRandomValues(arr);

	return `${prepend}${Array.from(arr).map(toHex).join("")}`;
}

export const makeHash = (stack) => {
	if (stack.length <= 0) return 0;

	return Array.from(`${stack}`).reduce((hash, char) => {
		hash = ((hash<<5)-hash)+char.charCodeAt(0);
		return hash & hash;
	}, 0);
}

export const hexHash = (stack) => toHex(makeHash(stack));

