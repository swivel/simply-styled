/**
 *  styled-js: A dependency-free CSS-in-JS library
 *
 *  Copyright (C) 2020  Joseph Dalrymple
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/
import { hexHash } from "./utils.js";

export default class StyleContext {
	static generateContextId(className = "") {
		const stack = (new Error()).stack;
		return hexHash(`${className}${stack}`);
	}

	constructor(props, styleClass, context = StyleContext.generateContextId(styleClass.className)) {
		this.elem = null;
		this.parent = styleClass;
		this.context = context;
		this.props = props;

		this._render();
	}

	update(props = {}) {
		this.props = props;
		this._render();
	}

	_render() {
		const { parent, context, props } = this;

		const newElem = document.createElement("style");

		const { className } = parent;
		newElem.innerHTML = `.${this} \{\n${parent._arrToCSS(parent.styles, props)}\n\}`;

		for (let [sel, styles] of parent.children) {
			const selector = parent._formatSelector(sel, this);
			newElem.innerHTML += `\n\n${selector} \{\n${parent._arrToCSS(styles, props)}\n\}`;
		}

		if (document.body.contains(this.elem)) {
			this.elem.replaceWith(newElem);
		} else {
			document.body.appendChild(newElem);
		}

		this.elem = newElem;
	}

	toString() {
		return `${this.parent.className}_${this.context}`;
	}
}


