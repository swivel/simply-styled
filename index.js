/**
 *  styled-js: A dependency-free CSS-in-JS library
 *
 *  Copyright (C) 2020  Joseph Dalrymple
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

import StyleClass from "./style-class.js";
import { genClassName } from "./utils.js";

const styled = (name, ...extra) => {
	const parent = name instanceof StyleClass ? name : false;

	let prefix;

	if (parent) {
		prefix = parent.className;
	} else if (!name.raw) {
		prefix = name;
	}

	const className = genClassName(prefix);

	const tag = (css, ...props) =>
		new StyleClass(className, css.reduce((s, c, i) => ([
			...s, c, ...([
				props[i] ?? ""
			])
		]), []), parent);

	return name.raw ? tag(name, ...extras) : tag;
}

export default styled;
